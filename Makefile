# include your own private variables for custom deployment configuration
-include PrivateRules.mak

GITLAB_API_PRIVATE_TOKEN ?=
GITLAB_API_REQUESTER ?=
NEXUS_URL ?= 
NEXUS_API_PASSWORD ?=
NEXUS_API_USERNAME ?=
NEXUS_DOCKER_QUARANTINE_REPO ?=
MAIN_DOCKER_REGISTRY_HOST ?=
MONGO_URL ?=
MONGO_DB ?=
MONGO_COL ?=

PROJECT = ska-skadc-backend

KUBE_NAMESPACE = ska-skadc

HELM_RELEASE = $(PROJECT)

# include makefile targets for Python packages management
-include .make/python.mk

# include makefile targets for Helm Charts management
-include .make/k8s.mk

# include makefile targets for Oci images management
-include .make/oci.mk

# include makefile targets for Helm management
-include .make/helm.mk

## The following should be standard includes
-include .make/base.mk


K8S_CHART_PARAMS = \
	--set global.env.gitlab_api_requester=$(GITLAB_API_REQUESTER) \
	--set global.env.gitlab_api_private_token=$(GITLAB_API_PRIVATE_TOKEN) \
	--set global.storageClass=$(STORAGE)\
	--set global.env.nexus_url=$(NEXUS_URL) \
	--set global.env.nexus_api_username=$(NEXUS_API_USERNAME) \
	--set global.env.nexus_api_password=$(NEXUS_API_PASSWORD) \
	--set global.env.mongo_db=$(MONGO_DB) \
	--set global.env.mongo_col=$(MONGO_COL) \
	--set global.env.mongo_url='$(MONGO_URL)' \
	--set global.env.main_docker_registry_host=$(MAIN_DOCKER_REGISTRY_HOST) 



run_code_helm:
	@export NEXUS_URL=${NEXUS_URL}; python3 src/app/models/helm_dependencies.py 

run_code_python:
	@export NEXUS_URL=${NEXUS_URL}; python3 src/app/models/python_dependencies.py

run_code_fastapi:
	@export NEXUS_URL=${NEXUS_URL}; export NEXUS_API_PASSWORD=${NEXUS_API_PASSWORD}; export NEXUS_API_USERNAME=${NEXUS_API_USERNAME}; \
	export NEXUS_DOCKER_QUARANTINE_REPO=${NEXUS_DOCKER_QUARANTINE_REPO};  export MAIN_DOCKER_REGISTRY_HOST=${MAIN_DOCKER_REGISTRY_HOST}; \
	export GITLAB_API_PRIVATE_TOKEN=${GITLAB_API_PRIVATE_TOKEN}; export GITLAB_API_REQUESTER=${GITLAB_API_REQUESTER}; \
	export MONGO_URL=${MONGO_URL}; export MONGO_DB=${MONGO_DB}; export MONGO_COL=${MONGO_COL}; \
	python3 src/main.py

docker_development:
	docker run  --env-file ./.env --expose=8000 --net=host -it artefact.skao.int/ska-skadc-backend:0.1.0-dirty /bin/bash