import io
import re
import tarfile

import yaml
from packaging import version
from ska_cicd_artefact_validations.validation.controllers.component_manager import (  # NOQA: E501
    HelmHandler,
)
from ska_cicd_artefact_validations.validation.models.component import Component

from app.models.common import search_all_components, search_components


class HelmDependencies(HelmHandler):
    async def get_helm_dependencies(self, asset, latest_version_list_helm):
        helm_dependencies = []

        with tarfile.open(asset, "r") as archive:
            helm_dependencies_file = [
                file
                for file in archive.getmembers()
                if "Chart.yaml" in file.name
            ]
            if helm_dependencies_file:
                with io.TextIOWrapper(
                    archive.extractfile(helm_dependencies_file[0]),
                    encoding="utf-8",
                ) as fd:
                    yaml_file = yaml.load(fd, Loader=yaml.Loader)
                    if "dependencies" in yaml_file:
                        for dependencies in yaml_file["dependencies"]:
                            if "ska" in dependencies["name"]:
                                # use latest_version_list_helm to optimize code
                                if latest_version_list_helm:
                                    latest_version = latest_version_list_helm[
                                        dependencies["name"]
                                    ]
                                else:
                                    (
                                        _,
                                        latest_version,
                                    ) = await search_components(
                                        "helm-internal",
                                        dependencies["name"],
                                        self.api,
                                    )
                                    latest_version = max(
                                        latest_version, default=0
                                    )
                            else:
                                latest_version = "latest"

                            if "repository" in dependencies:
                                if "engageska" in dependencies["repository"]:
                                    deprecated = True
                                    latest_version = 0
                                else:
                                    deprecated = False

                            helm_dependencies.append(
                                {
                                    "format": "helm",
                                    "name": dependencies["name"],
                                    "version": dependencies["version"],
                                    "latest_version": latest_version,
                                    "deprecated": deprecated,
                                }
                            )

        return helm_dependencies

    def find_key(self, dict_file, key):
        if isinstance(dict_file, dict):
            yield from self.iter_dict(dict_file, key, [])
        elif isinstance(dict_file, list):
            yield from self.iter_list(dict_file, key, [])

    def iter_dict(self, dict_file, key, indices):

        for iter_key, iter_value in dict_file.items():

            if iter_key == key:
                dict_file.pop(iter_key)
                if "image" in dict_file:
                    oci_dependency_name = dict_file["image"]

                    if "registry" in dict_file:
                        oci_dependency_registry = dict_file["registry"]

                        if "engageska" in oci_dependency_registry:
                            deprecated = True
                        else:
                            deprecated = False

                        if "/" in oci_dependency_registry:
                            # if the path to the package is in the registry variable # NOQA: E501
                            oci_dependency_registry = oci_dependency_registry[
                                oci_dependency_registry.find("/")
                                + 1 :  # NOQA: E203
                            ]
                            oci_dependency_name = (
                                oci_dependency_registry
                                + "/"
                                + oci_dependency_name
                            )
                    else:
                        deprecated = False

                    if "/" in oci_dependency_name:
                        # if artefact.skao.int in name remove it and leav just the package name # NOQA: E501
                        oci_dependency_name = oci_dependency_name[
                            oci_dependency_name.find("/") + 1 :  # NOQA: E203
                        ]

                    yield iter_value, oci_dependency_name, deprecated
                else:
                    yield iter_value, dict_file["repository"], ""
            if isinstance(iter_value, dict):
                yield from self.iter_dict(
                    iter_value, key, indices + [iter_key]
                )
            elif isinstance(iter_value, list):
                yield from self.iter_list(
                    iter_value, key, indices + [iter_key]
                )

    def iter_list(self, seq, key, indices):
        for k, v in enumerate(seq):
            if isinstance(v, dict):
                yield from self.iter_dict(v, key, indices + [k])
            elif isinstance(v, list):
                yield from self.iter_list(v, key, indices + [k])

    async def get_oci_dependencies(self, asset):

        oci_dependencies = []
        oci_names_list = []

        with tarfile.open(asset, "r") as archive:
            oci_dependencies_file = [
                file
                for file in archive.getmembers()
                if "values.yaml" in file.name
            ]

            if oci_dependencies_file:
                with io.TextIOWrapper(
                    archive.extractfile(oci_dependencies_file[0]),
                    encoding="utf-8",
                ) as fd:
                    yaml_file = yaml.load(fd, Loader=yaml.Loader)

                    keys_exist = True

                    while keys_exist is True:
                        try:
                            (
                                oci_dependency_tag,
                                oci_dependency_name,
                                deprecated,
                            ) = next(self.find_key(yaml_file, "tag"))
                            if oci_dependency_name not in oci_names_list:

                                if oci_dependency_tag == "":
                                    oci_dependency_tag = "latest"

                                if ".Values" not in oci_dependency_name:
                                    if "ska" in oci_dependency_name:
                                        if oci_dependency_tag == "latest":
                                            deprecated = True

                                        else:
                                            (
                                                _,
                                                latest_version,
                                            ) = await search_components(
                                                "docker-internal",
                                                oci_dependency_name,
                                                self.api,
                                            )
                                            latest_version = max(
                                                latest_version, default=0
                                            )
                                    else:
                                        latest_version = "latest"

                                    if deprecated is True:
                                        latest_version = 0
                                    oci_names_list.append(oci_dependency_name)
                                    oci_dependencies.append(
                                        {
                                            "format": "oci",
                                            "name": oci_dependency_name,
                                            "version": oci_dependency_tag,
                                            "latest_version": latest_version,
                                            "deprecated": deprecated,
                                        }
                                    )
                        except Exception as e:
                            if e:
                                print("Expection", e)
                            keys_exist = False

                    return oci_dependencies

    async def get_list_helm_car(self):

        result = await search_all_components("helm-internal", self.api)
        return result

    async def get_artefact_info(
        self, artefact_name, artefact_version, latest_version_list_helm
    ):

        print("N: ", artefact_name, "V: ", artefact_version)
        result, _ = await search_components(
            "helm-internal", artefact_name, self.api
        )

        for index in result:
            if result[index]["version"] == artefact_version:
                component = Component(
                    {
                        "component": {
                            "componentId": result[index]["id"],
                            "name": artefact_name,
                            "version": artefact_version,
                            "format": "helm",
                        },
                        "repositoryName": "helm-internal",
                    }
                )
                assets = await self.download(component)
                print("downloaded")
                component._assets = assets
                for asset in assets:
                    if re.search(r".tgz", asset):
                        dependencies = await self.get_helm_dependencies(
                            asset, latest_version_list_helm
                        )
                        print("Helm dependencies set")
                        oci_dependecies = await self.get_oci_dependencies(
                            asset
                        )
                        print("OCI dependencies set")
                        if oci_dependecies:
                            dependencies.extend(oci_dependecies)

                await self.delete_downloaded_components(component)

        return {
            "format": "helm",
            "name": artefact_name,
            "version": artefact_version,
            "dependencies": dependencies,
        }

    async def get_latest_version_list(self, list_all_artefacts):
        list_latest_version = {}

        for artefact in list_all_artefacts:
            if artefact["name"] not in list_latest_version or version.parse(
                list_latest_version[artefact["name"]]
            ) < version.parse(artefact["version"]):
                list_latest_version.update(
                    {artefact["name"]: artefact["version"]}
                )


# async def main():
#     import os

#     import aiohttp
#     from ska_cicd_services_api import nexus

#     url = os.getenv("NEXUS_URL")
#     print(url)
#     artefact_name_list = [
#         "ska-mvp-mid",
#         "ska-cicd-artefact-validations",
#         "ska-tango-examples",
#         "ska-tmc-low",
#         "ska-tmc-mid",
#         "ska-tango-base",
#     ]  # NOQA: E501
#     artefact_version_list = [
#         "0.7.0",
#         "0.4.0",
#         "0.4.16",
#         "0.3.8",
#         "0.4.1",
#         "0.2.23",
#     ]  # NOQA: E501

#     async with aiohttp.ClientSession() as session:
#         api = nexus.NexusApi(session, url=url)
#         helm = HelmDependencies(api)
#         task = await helm.get_artefact_info(
#             "ska-tango-base", "0.2.23", None
#         )  # NOQA: E501
#         print(task)
#         # list_all_artefacts = await  helm.get_list_helm_car()
#         # list_latest_version = await helm.get_latest_version_list(list_all_artefacts) # NOQA: E501


# if __name__ == "__main__":
#     import asyncio

#     asyncio.run(main())
