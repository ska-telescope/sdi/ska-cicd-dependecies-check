import pymongo
from pymongo.mongo_client import MongoClient


class MongoDBApi:
    """
    A class used to interact with MongoDB
    with the module pymongo by initializing it with a pymongo client
    https://docs.mongodb.com/manual/reference/method/js-collection/

    This class can also be used to test by initializing it with
    a mongomock client
    https://github.com/mongomock/mongomock/blob/develop/tests/test__collection_api.py

    On the find methods we pass {"_id":0} so they do not return the ObjectId.
    If the ObjectId is returned through Celery a Serialization exception occurs
    """

    def __init__(self, client: MongoClient):

        self.client = client

    def select_database(self, name):
        self.db = self.client[name]

    def select_collection(self, name):
        self.mycol = self.db[name]

    def list_all_collections(self):

        return self.db.list_collection_names()

    def list_all_documents(self):

        return list(self.mycol.find({}, {"_id": 0}))

    def add_artifact_metadata(self, metadata):

        self.mycol.insert_many(metadata)

    def delete_documents(self, field_name, document_value):
        """
        field_name corresponds to the field that is going to be
        compared the document_value to be deleted

        for example field_name = '_id'
        and document_value = ObjectId(6089da887ee2c6ab49fe4a23)

        The only field that is always different beetween the
        documents is de _id field.
        """

        return self.mycol.delete_one({field_name: document_value})

    def find_outdated_documents(self, age):

        return list(self.mycol.find({"age": {"$gt": age}}, {"_id": 0}))

    def find_documents(self, query_filter: dict):

        return list(self.mycol.find(query_filter, {"_id": 0}))

    def find_documents_sort(
        self,
        query_filter: dict,
        key: str,
        limit: int = 0,
        direction=pymongo.DESCENDING,
    ):

        return list(
            self.mycol.find(query_filter, {"_id": 0})
            .sort(key, direction)
            .limit(limit)
        )

    def get_collection_size(self):

        return self.mycol.count_documents({})

    def find_documents_index(self, pattern):

        resp = self.mycol.find(
            {"name": {"$regex": pattern, "$options": "i"}}
        ).sort([("name", pymongo.ASCENDING), ("version", pymongo.DESCENDING)])
        search_result = []
        for document in resp:
            search_result.append(
                {
                    "name": document["name"],
                    "version": document["version"],
                    "format": document["format"],
                }
            )

        return search_result
