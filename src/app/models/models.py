from pydantic import BaseModel


class HelmDependenciesModel(BaseModel):
    format: str = None
    name: str = None
    version: str = None
