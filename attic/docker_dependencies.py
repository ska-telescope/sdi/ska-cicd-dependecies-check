import aiohttp
from ska_cicd_services_api import nexus
from ska_cicd_services_api import gitlab
import io
import logging
import os
import re
import docker
import asyncio
import base64



required_metadata_fields = [
    "CI_COMMIT_AUTHOR",
    "CI_COMMIT_REF_NAME",
    "CI_COMMIT_REF_SLUG",
    "CI_COMMIT_SHA",
    "CI_COMMIT_SHORT_SHA",
    "CI_COMMIT_TIMESTAMP",
    "CI_JOB_ID",
    "CI_JOB_URL",
    "CI_PIPELINE_ID",
    "CI_PIPELINE_IID",
    "CI_PIPELINE_URL",
    "CI_PROJECT_ID",
    "CI_PROJECT_PATH_SLUG",
    "CI_PROJECT_URL",
    "CI_REPOSITORY_URL",
    "CI_RUNNER_ID",
    "CI_RUNNER_REVISION",
    "CI_RUNNER_TAGS",
    "GITLAB_USER_NAME",
    "GITLAB_USER_EMAIL",
    "GITLAB_USER_LOGIN",
    "GITLAB_USER_ID",
]

async def get_list_from_async_gen(async_generator):
    return [i async for i in async_generator]

class DockerHandler:
    def __init__(self) -> None:
        self.client = docker.from_env()
        self.client.login(
            username=os.getenv("NEXUS_API_USERNAME"),
            password=os.getenv("NEXUS_API_PASSWORD"),
            registry=os.getenv("NEXUS_DOCKER_QUARANTINE_REPO"),
        )

    async def search_components(self,nexus_repository, artefact_name):
        result = {}
        counter = 0
        async with aiohttp.ClientSession() as session:
            url = os.getenv("NEXUS_URL")
            logging.debug("NEXUS URL: %s", url)
            api = nexus.NexusApi(session, url=url)
            artefacts = await api.get_components_list(nexus_repository)
            async for art in artefacts:
                if art["name"] == artefact_name:
                    d1 = { str(counter): {
                        "id": art["id"],
                        "format": art["format"],
                        "version": art["version"]
                    }}
                    result.update(d1)
                    counter += 1
        return result

    async def download(self, artefact_name, artefact_version):
        docker_host = os.getenv("MAIN_DOCKER_REGISTRY_HOST")
        if docker_host:
            image_path = (
                f"{docker_host}/" f"{artefact_name}:" f"{artefact_version}"
            )
        else:
            image_path = f"{artefact_name}:" f"{artefact_version}"
        image = self.client.images.pull(image_path)
        return image


    async def get_metadata(self, image):
        labels = {}
        for layer in reversed(image.history()):
            # reverse and pull out LABELs
            if " LABEL " in layer["CreatedBy"]:
                label = layer["CreatedBy"].split(" LABEL ")[-1].strip()
                if "=" in label:
                    key, val = label.split("=", 1)
                    labels[key] = val
            else:
                labels = {}

        metadata_keys = []

        for label in list(labels):
            if labels[label].find("=") > 0:
                for field in required_metadata_fields:
                    if field in labels[label] or field == label:
                        metadata_keys.append(field)
            else:
                if label in required_metadata_fields:
                    metadata_keys.append(label)

        if len(metadata_keys) == len(required_metadata_fields):
            return image.labels

        else:
            logging.error(
                "The required labels found are the following: %s",
                str(metadata_keys),
            )
            return None

    async def get_oci_dependencies(self, proj_id, artefact_name, artefact_version):

        oci_dependencies = {}


        async with aiohttp.ClientSession() as session:
            gitlab_api = gitlab.GitLabApi(session)
            repo_tree = await gitlab_api.get_repo_tree(proj_id)

            repo_tree = await get_list_from_async_gen(repo_tree)

            for files in repo_tree:
                # print(files)
                if files["name"] == "Dockerfile":
                    try:
                        file = await gitlab_api.get_file_from_repository(proj_id, "Dockerfile", artefact_version )

                    except:
                        file = await gitlab_api.get_file_from_repository(proj_id, "Dockerfile", "main")

                    content = self.decode_file(file)

                    for line in content:
                       dependencies = self.parse_lines(line)
                       if dependencies:
                        oci_dependencies.update(dependencies)
                        dependencies = None
                        

                elif files["name"] == "images" and files["type"] == "tree":
                    try:
                        file = await gitlab_api.get_file_from_repository(proj_id,"images%2F"+ str(artefact_name) +"%2FDockerfile", artefact_version )

                    except:
                        file = await gitlab_api.get_file_from_repository(proj_id,"images%2F"+ str(artefact_name) +"%2FDockerfile", "master" )

                    content = self.decode_file(file)

                    for line in content:
                       dependencies = self.parse_lines(line)
                       if dependencies:
                        oci_dependencies.update(dependencies)
                        dependencies = None

            return oci_dependencies
    
    def decode_file(self, file):

        base64_bytes = file["content"].encode('ascii')

        message_bytes = base64.b64decode(base64_bytes)

        content = message_bytes.decode('ascii')

        content = content.split("\n")

        return content

    def parse_lines(self, line):

        dependencies = None

        if "BASE_IMAGE=" in line:
            dependencie_name = (line.split('"')[1]).split(":")[0]
            dependencie_version = ((line.split(" ")[1]).split(":")[1]).replace('"', "")
            if "/" in dependencie_name:
                dependencie_name = dependencie_name.split("/")[1]
            if "${CAR_OCI_REGISTRY_HOST}" in dependencie_name:
                dependencie_name = dependencie_name.replace("CAR_OCI_REGISTRY_HOST", "artefact.skao.int")[1]
            dependencies = { dependencie_name : dependencie_version }

        elif "BUILD_IMAGE=" in line:
            dependencie_name = (line.split('"')[1]).split(":")[0]
            dependencie_version = ((line.split(" ")[1]).split(":")[1]).replace('"', "")
            if "/" in dependencie_name:
                dependencie_name = dependencie_name.split("/")[1]
            if "${CAR_OCI_REGISTRY_HOST}" in dependencie_name:
                dependencie_name = dependencie_name.replace("CAR_OCI_REGISTRY_HOST", "artefact.skao.int")[1]
            dependencies = { dependencie_name : dependencie_version }

        elif "FROM" in line and not "$" in line:
            dependencie_name = (line.split(" ")[1]).split(":")[0]
            dependencie_version = (line.split(" ")[1]).split(":")[1]
            if "${\CAR_OCI_REGISTRY_HOST}" in dependencie_name:
                dependencie_name = dependencie_name.replace("CAR_OCI_REGISTRY_HOST", "artefact.skao.int")[1]
            
            dependencies = { dependencie_name : dependencie_version }
        
        return dependencies



async def main():
    docker = DockerHandler()

    artefact_name = ["ska-tango-examples","ska-tango-images-pytango-builder","ska-tmc-centralnode","ska-sdp-opinterface","ska-mid-cbf-mcs", "ska-ser-skallop"]
    artefact_version = ["0.4.16","9.3.16","0.3.6","0.2.1","0.5.5","2.7.10"]
    

    for index in range(0,len(artefact_name)):

        print("")
        print("ARTEFACTS: ", artefact_name[index])

        image = await docker.download(artefact_name[index],artefact_version[index])

        labels = await docker.get_metadata(image)

        oci_dependecies = await docker.get_oci_dependencies(labels["CI_PROJECT_ID"], artefact_name[index], artefact_version[index])

        print("OCI Dependencies: ")
        for dependecy_name in oci_dependecies:
            print("Name: ",dependecy_name, "  Version:", oci_dependecies[dependecy_name])

        print("OCI Dependencies: ")
    
if __name__ == "__main__":
   asyncio.run(main())